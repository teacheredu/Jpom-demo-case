package cn.keepbx.controller;

import io.jboot.web.controller.JbootController;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * @author jiangzeyin
 * @date 2019/3/13
 */
@RequestMapping("/")
public class TestController extends JbootController {

    public void index() {
        renderText("hello jboot");

    }
}
