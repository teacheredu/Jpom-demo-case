package cn.keepbx;

import cn.jiangzeyin.common.ApplicationBuilder;
import cn.jiangzeyin.common.EnableCommonBoot;
import cn.jiangzeyin.common.validator.ParameterInterceptor;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jiangzeyin
 * @date 2018/5/10
 */
@SpringBootApplication()
@EnableCommonBoot
public class SpringBootApp {
    public static void main(String[] args) throws Exception {
        ApplicationBuilder.createBuilder(SpringBootApp.class).addInterceptor(ParameterInterceptor.class).run(args);
    }
}
